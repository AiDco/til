import Vapor
import Fluent

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    
    /// Register Acromyms.routers
    let acronymsController = AcronymsController()
    try router.register(collection: acronymsController)
    
    /// Register Users.routers
    let usersController = UserController()
    try router.register(collection: usersController)
    
    /// Register Categories.routers
    let categoriesController = CategoriesController()
    try router.register(collection: categoriesController)
    
    ///Register Index
    let websiteController = WebsiteController()
    try router.register(collection: websiteController)
    
}
